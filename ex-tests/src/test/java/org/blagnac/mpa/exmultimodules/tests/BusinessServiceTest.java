package org.blagnac.mpa.exmultimodules.tests;

import org.blagnac.mpa.exmultimodules.model.Person;
import org.blagnac.mpa.exmultimodules.service.BusinessService;
import org.junit.Assert;
import org.junit.Test;

public class BusinessServiceTest {

	@Test
	public void test_addPerson() {
		Person personToAdd = new Person("TEST", "Test");

		BusinessService.addPerson(personToAdd);

		Assert.assertTrue(BusinessService.getPersons().contains(personToAdd));
	}
}
