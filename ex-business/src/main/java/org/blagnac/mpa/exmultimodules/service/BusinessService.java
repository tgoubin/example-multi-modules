package org.blagnac.mpa.exmultimodules.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.blagnac.mpa.exmultimodules.data.DataStorage;
import org.blagnac.mpa.exmultimodules.model.Person;

public class BusinessService {

	public static List<Person> getPersons() {
		return new ArrayList<Person>(Arrays.asList(DataStorage.DATA));
	}

	public static void addPerson(Person person) {
		List<Person> personList = getPersons();
		personList.add(person);
		Person[] personArray = new Person[DataStorage.DATA.length + 1];
		DataStorage.DATA = personList.toArray(personArray);
	}
}
