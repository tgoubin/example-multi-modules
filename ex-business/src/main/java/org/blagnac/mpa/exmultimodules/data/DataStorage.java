package org.blagnac.mpa.exmultimodules.data;

import org.blagnac.mpa.exmultimodules.model.Person;

public class DataStorage {

	public static Person[] DATA = new Person[] { new Person("JORDAN", "Michael"), new Person("BRYANT", "Kobe"),
			new Person("PARKER", "Tony") };
}
