package org.blagnac.mpa.exmultimodules.ui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.blagnac.mpa.exmultimodules.model.Person;
import org.blagnac.mpa.exmultimodules.service.BusinessService;

public class ButtonPanel extends JPanel {

	private static final long serialVersionUID = 8282813473310517522L;

	private ListPanel listPanel;

	private JButton btAddElementToList;

	public ButtonPanel(ListPanel listPanel) {
		this.listPanel = listPanel;

		setPreferredSize(new Dimension(500, 50));
		setLayout(new FlowLayout(FlowLayout.CENTER));

		btAddElementToList = new JButton("Add an element to the list");
		add(btAddElementToList);

		onClickBtAddElementToList();
	}

	private void onClickBtAddElementToList() {
		btAddElementToList.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String name = JOptionPane.showInputDialog(getParent(), "Name?");

				if (name != null) {
					String firstName = JOptionPane.showInputDialog(getParent(), "First name?");

					if (firstName != null) {
						// Calls the business part of the application to add a person in the data
						BusinessService.addPerson(new Person(name, firstName));

						// Invoke the JList to be refreshed
						listPanel.displayList();
					}
				}
			}
		});
	}
}
