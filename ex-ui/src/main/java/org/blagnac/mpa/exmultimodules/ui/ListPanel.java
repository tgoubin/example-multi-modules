package org.blagnac.mpa.exmultimodules.ui;

import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.blagnac.mpa.exmultimodules.model.Person;
import org.blagnac.mpa.exmultimodules.service.BusinessService;

public class ListPanel extends JPanel {

	private static final long serialVersionUID = 3376121383365761637L;

	private JList<String> exampleList;

	public ListPanel() {
		setLayout(new FlowLayout(FlowLayout.CENTER));

		exampleList = new JList<String>();
		exampleList.setModel(new ExampleListModel());
		add(new JScrollPane(exampleList));

		displayList();
	}

	public void displayList() {
		ExampleListModel listModel = (ExampleListModel) exampleList.getModel();

		// Updates the JList model - not yet the JList displayed
		listModel.setData(BusinessService.getPersons());

		// Updates the JList
		exampleList.updateUI();
	}

	private static class ExampleListModel extends AbstractListModel<String> {

		private static final long serialVersionUID = -4506775634919525060L;

		private List<Person> data;

		public ExampleListModel() {
			data = new ArrayList<>();
		}

		public void setData(List<Person> data) {
			this.data = data;
		}

		@Override
		public int getSize() {
			return data.size();
		}

		@Override
		public String getElementAt(int index) {
			return data.get(index).toString();
		}
	}
}
