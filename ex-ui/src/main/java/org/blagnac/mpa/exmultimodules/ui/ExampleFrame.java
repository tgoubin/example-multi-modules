package org.blagnac.mpa.exmultimodules.ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class ExampleFrame extends JFrame {

	private static final long serialVersionUID = 3799709552966585541L;

	public ExampleFrame() {
		setTitle("Example multi-modules application");
		setSize(500, 300);
		setLocationRelativeTo(getParent());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel mainPanel = new JPanel(new BorderLayout());
		setContentPane(mainPanel);
		ListPanel listPanel = new ListPanel();
		mainPanel.add(new ButtonPanel(listPanel), BorderLayout.NORTH);
		mainPanel.add(listPanel, BorderLayout.CENTER);
		
		setVisible(true);
	}
}
